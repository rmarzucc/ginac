/** @file GPath.h
 *
 *  Interface to GiNaC's symbolic pathintegral. */

/*
 *  GiNaC Copyright (C) 1999-2020 Johannes Gutenberg University Mainz, Germany
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GINAC_GPath_H	//Need to rename this also! GPath
#define GINAC_GPath_H	//Need to rename this also! GPath

#include "basic.h"
#include "ex.h"
#include "archive.h"
#include "lst.h"
#include "constant.h"
#include "pintegral.h"

namespace GiNaC {

/** Elliptic Multiple Polylogarithm. */
class GPath : public basic
{
	GINAC_DECLARE_REGISTERED_CLASS(GPath, basic)
	
	// other constructors
public:
	GPath(const ex & args_, const ex & z_, const ex & path_);
	
	// functions overriding virtual functions from base classes
public:
	unsigned precedence() const override {return 45;}
	ex eval() const override;
	ex evalf() const override;
	//int degree(const ex & s) const override;
	//int ldegree(const ex & s) const override;
	//ex eval_ncmul(const exvector & v) const override;
	size_t nops() const override;
	ex op(size_t i) const override;
	ex & let_op(size_t i) override;
	//ex expand(unsigned options = 0) const override;
	//exvector get_free_indices() const override;
	//unsigned return_type() const override;
	//return_type_t return_type_tinfo() const override;
	ex conjugate() const override;
	//ex eval_integ() const override;
	/** Save (a.k.a. serialize) object into archive. */
	void archive(archive_node& n) const override;
	/** Read (a.k.a. deserialize) object from archive. */
	void read_archive(const archive_node& n, lst& syms) override;
	ex Nqexpand(ex points, int ord, unsigned options = 0) const;
	ex dec_path() const;
	lst int_part0(const int nn, const int mm) const;
protected:
	ex derivative(const symbol & s) const override;
	exvector lst_to_vec(const lst list) const;
	void vec_to_lst(const exvector vec, lst l) const;
	lst reverse_list(const lst l) const;
	lst int_part0Test(const int nn, const int mm) const;
	//ex series(const relational & r, int order, unsigned options = 0) const override;

	// new virtual functions which can be overridden by derived classes
	
	// non-virtual functions in this class
protected:
	void do_print(const print_context & c, unsigned level) const;
	void do_print_latex(const print_latex & c, unsigned level) const;
public:
	// none
private:
	ex args;
	ex z;
	ex path;
};
GINAC_DECLARE_UNARCHIVER(GPath); 

// utility functions

// GiNaC::ex pfCoeff(
// 	const GiNaC::ex &k,
// 	const GiNaC::ex &n,
// 	const GiNaC::ex &a,
// 	const GiNaC::ex &m,
// 	const GiNaC::ex &b,
// 	const GiNaC::ex &e
// );

} // namespace GiNaC

#endif // ndef GINAC_GPath_H
