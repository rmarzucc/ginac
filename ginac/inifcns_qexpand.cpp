/** @file inifcns_qExpand.cpp
 *
 *  Implementation of some special functions related to the q-expansion of elliptic polylogarithms sGt
 *
 *  The functions are:
 *    elliptic multiple polylogarithms defined on the torus tau        		sGt({{n1,z1},..},z,tau)
 *    q = exp(2*Pi*I*m/N)       											qq(N,m)
 *    the elliptic kernel g on the torus tau                            	sgt(n,z,tau)
 *    expansion coefficient of sgt optimised for integration over w=q(z)    sgtWCoeff(n,x,tau,k,ord)
 * 	  Polylogarithms G integrated iteratively along the points {a,..,b}		GPath({z1,..},x,{a,..,b})
 *
 *  Some remarks:
 *
 *
 */

/*
 *  GiNaC Copyright (C) 1999-2020 Johannes Gutenberg University Mainz, Germany
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "inifcns.h"

#include "add.h"
#include "constant.h"
#include "lst.h"
#include "mul.h"
#include "numeric.h"
#include "operators.h"
#include "power.h"
#include "pseries.h"
#include "relational.h"
#include "symbol.h"
#include "utils.h"
#include "wildcard.h"

#include "integration_kernel.h"
#include "utils_multi_iterator.h"

#include <cln/cln.h>
#include <sstream>
#include <stdexcept>
#include <vector>
#include <cmath>

using namespace std;

namespace GiNaC {


//////////////////////////////////////////////////////////////////////
//
// Complete elliptic integrals
//
// helper functions
//
//////////////////////////////////////////////////////////////////////


// anonymous namespace for helper function
namespace {




} // end of anonymous namespace


//////////////////////////////////////////////////////////////////////
//
// Complete elliptic integrals
//
// GiNaC function
//
//////////////////////////////////////////////////////////////////////

static ex q_evalf(const ex& N, const ex& m)
{
	ex newN = N.evalf();
	ex newm = m.evalf();

	if ( !(newN.info(info_flags::numeric)&&newm.info(info_flags::numeric)) ) {
		return qq(N,m).hold();
	}

	ex result = exp(2*Pi*I*m/N);

	return result.evalf();
}


static ex q_eval(const ex& N, const ex& m)
{
	// if (k == _ex0) {
	// 	return Pi/2;
	// }

	// if ( k.info(info_flags::numeric) && !k.info(info_flags::crational) ) {
	// 	return EllipticK(k).evalf();
	// }

	return qq(N,m).hold();
}


static ex q_deriv(const ex& N, const ex& m, unsigned deriv_param)
{
    GINAC_ASSERT(deriv_param<2);
	ex retval;
	
	// d/dN q(N,m) -> (-2*Pi*I*m*q(N,m))/pow(N,2)
	if (deriv_param==0)
		retval = (-2*Pi*I*m*qq(N,m))/pow(N,2);
	// d/dm q(N,m) -> (2*Pi*I*q(N,m))/N
	if (deriv_param==1)
		retval = (2*Pi*I*qq(N,m))/N;
	return retval;
}


static ex q_series(const ex& N, const ex& m, const relational& rel, int order, unsigned options)
{       
	const ex qExp = exp((2*Pi*I*m)/N);

	return qExp.series(rel, order, options);

	// all other cases
	throw do_taylor();
}

static void q_print_latex(const ex& N, const ex& m, const print_context& c)
{
	c.s << "\\q_{";
	N.print(c);
	c.s << "}^{";
	m.print(c);
	c.s << "}";
}


REGISTER_FUNCTION(qq,
                  evalf_func(q_evalf).
                  eval_func(q_eval).
                  derivative_func(q_deriv).
                  series_func(q_series).
                  print_func<print_latex>(q_print_latex).
                  do_not_evalf_params()
				  );



//////////////////////////////////////////////////////////////////////
//
// Iterated integrals
//
// helper functions
//
//////////////////////////////////////////////////////////////////////

static ex wcoeff_evalf(const ex& n, const ex& x, const ex& qtau, const ex& k, const ex& ord) 
{
	if ( !(n.info(info_flags::numeric)&&ord.info(info_flags::numeric)&&k.info(info_flags::numeric)) ) {
		return sgtWCoeff(n, x, qtau, k, ord).hold();
	}

	size_t kk = ex_to<numeric>(k).to_long();
	size_t oord = ex_to<numeric>(ord).to_long();
	numeric nn = ex_to<numeric>(n);

	ex hlp = 0;

	if (n == 1 && k == I)
	{
		hlp = -2*Pi*I;
		return hlp.evalf();
	}

	if (k == 0)
	{
		if (nn == 1)
		{
			hlp = I*Pi;
			return hlp.evalf();
		} else if (nn.is_even())
		{
			hlp = -2*zeta(n);
			return hlp.evalf();
		} else
		{
			cout << "n = " << n << "\n";
			return 0;
		}
		
	}

	if (k > 0)
	{
		size_t kk = ex_to<numeric>(k).to_long();
		for (size_t l = 1; l <= ord*pow(kk,-1); l++)
		{
			hlp += pow(l,n-1)*pow(qtau,kk*l);
		}

		hlp = hlp * (-1) * pow(qq(1,x),-k) * pow(2*Pi*I,n) * inverse(factorial(nn-1));
		
	}

	if (k < 0)
	{
		size_t kk = ex_to<numeric>(-k).to_long();
		for (size_t l = 1; l <= ord*pow(kk,-1); l++)
		{
			hlp += pow(l,n-1)*pow(qtau,kk*l);
		}

		hlp = hlp * (-1) * pow(-1,n) * pow(qq(1,x),-k) * pow(2*Pi*I,n) * inverse(factorial(nn-1));
	}

	hlp = hlp.evalf();

	return hlp;

}

// static ex wcoeff_evalf(const ex& n, const ex& x, const ex& tau, const ex& k, const ex& ord) 
// {
// 	if ( !(n.info(info_flags::numeric)&&ord.info(info_flags::numeric)&&k.info(info_flags::numeric)) ) {
// 		return sgtWCoeff(n, x, tau, k, ord).hold();
// 	}

// 	size_t kk = ex_to<numeric>(k).to_long();
// 	size_t oord = ex_to<numeric>(ord).to_long();
// 	numeric nn = ex_to<numeric>(n);

// 	ex hlp = 0;

// 	if (n == 1 && k == I)
// 	{
// 		hlp = 2*Pi*I;
// 		return hlp.evalf();
// 	}

// 	if (k == 0)
// 	{
// 		if (nn == 1)
// 		{
// 			hlp = I*Pi;
// 			return hlp.evalf();
// 		} else if (nn.is_even())
// 		{
// 			hlp = -2*zeta(n);
// 			return hlp.evalf();
// 		} else
// 		{
// 			cout << "n = " << n << "\n";
// 			return 0;
// 		}
		
// 	}

// 	if (kk > 0)
// 	{
// 		for (size_t l = 1; l <= ex_to<numeric>(ord*pow(k,-1)); l++)
// 		{
// 			hlp += pow(l,n-1)*pow(qq(1,tau),kk*l);
// 		}

// 		hlp = hlp * (-1) * pow(qq(1,x),-k) * pow(2*Pi*I,n) * inverse(factorial(nn-1));
		
// 	}

// 	if (kk < 0)
// 	{
// 		for (size_t l = 1; l <= ex_to<numeric>(ord*pow(k,-1)); l++)
// 		{
// 			hlp += pow(l,n-1)*pow(qq(1,tau),-kk*l);
// 		}

// 		hlp = hlp * pow(-1,n) * pow(qq(1,x),-k) * pow(2*Pi*I,n) * inverse(factorial(nn-1));
		
// 	}

// 	hlp = hlp.evalf();

// 	return hlp;

// }


static ex wcoeff_eval(const ex& n, const ex& x, const ex& tau, const ex& k, const ex& ord) 
{
	// if (k == _ex0) {
	// 	return Pi/2;
	// }

	// if ( k.info(info_flags::numeric) && !k.info(info_flags::crational) ) {
	// 	return EllipticK(k).evalf();
	// }

	return sgtWCoeff(n,x,tau,k,ord).hold();
}


static ex wcoeff_deriv(const ex& n, const ex& x, const ex& tau, const ex& k, const ex& ord, unsigned deriv_param) 
{
    GINAC_ASSERT(deriv_param > 0 && deriv_param < 3);
	ex retval;
	symbol xx;
	
	// d/dN q(N,m) -> (-2*Pi*I*m*q(N,m))/pow(N,2)
	if (deriv_param==1)
		retval = sgtWCoeff(n,xx,tau,k,ord).evalf().diff(xx,1).subs(xx==x);
	// d/dm q(N,m) -> (2*Pi*I*q(N,m))/N
	if (deriv_param==2)
		retval = sgtWCoeff(n,x,xx,k,ord).evalf().diff(xx,1).subs(xx==tau);
	return retval;
}


static ex wcoeff_series(const ex& n, const ex& x, const ex& tau, const ex& k, const ex& ord, const relational& rel, int order, unsigned options)
{       
	const ex retvalue = sgtWCoeff(n,x,tau,k,ord).evalf();

	return retvalue.series(rel, order, options);

	// all other cases
	throw do_taylor();
}

static void wcoeff_print_latex(const ex& n, const ex& x, const ex& tau, const ex& k, const ex& ord, const print_context& c)
{
	c.s << "\\C_{";
	k.print(c);
	c.s << "}^{";
	ord.print(c);
	c.s << "}(";
	n.print(c);
	c.s << ",";
	x.print(c);
	c.s << ";";
	tau.print(c);
	c.s << "}(";
}


// REGISTER_FUNCTION(GPath,
//                   dummy()
// 				  );

REGISTER_FUNCTION(ssgt,
                  dummy()
				  );

REGISTER_FUNCTION(sgtWCoeff,
                  evalf_func(wcoeff_evalf).
                  eval_func(wcoeff_eval).
                  derivative_func(wcoeff_deriv).
                  series_func(wcoeff_series).
                  print_func<print_latex>(wcoeff_print_latex).
                  do_not_evalf_params()
				  );

// // anonymous namespace for helper function
// namespace {

// // performs the actual series summation for an iterated integral
// cln::cl_N iterated_integral_do_sum(const std::vector<int> & m, const std::vector<const integration_kernel *> & kernel, const cln::cl_N & lambda, int N_trunc)
// {
//         if ( cln::zerop(lambda) ) {
// 		return 0;
// 	}

// 	cln::cl_F one = cln::cl_float(1, cln::float_format(Digits));

// 	const int depth = m.size();

// 	cln::cl_N res = 0;
// 	cln::cl_N resbuf;
// 	cln::cl_N subexpr;

// 	if ( N_trunc == 0 ) {
// 		// sum until precision is reached
// 		bool flag_accidental_zero = false;

// 		int N = 1;

// 		do {
// 			resbuf = res;

// 			if ( depth > 1 ) {
// 				subexpr = 0;
// 				multi_iterator_ordered_eq<int> i_multi(1,N+1,depth-1);
// 				for( i_multi.init(); !i_multi.overflow(); i_multi++) {
// 					cln::cl_N tt = one;
// 					for (int j=1; j<depth; j++) {
// 						if ( j==1 ) {
// 							tt = tt * kernel[0]->series_coeff(N-i_multi[depth-2]) / cln::expt(cln::cl_I(i_multi[depth-2]),m[1]);
// 						}
// 						else {
// 							tt = tt * kernel[j-1]->series_coeff(i_multi[depth-j]-i_multi[depth-j-1]) / cln::expt(cln::cl_I(i_multi[depth-j-1]),m[j]);
// 						}
// 					}
// 					tt = tt * kernel[depth-1]->series_coeff(i_multi[0]);
// 					subexpr += tt;
// 				}
// 			}
// 			else {
// 				// depth == 1
// 				subexpr = kernel[0]->series_coeff(N) * one;
// 			}
// 			flag_accidental_zero = cln::zerop(subexpr);
// 			res += cln::expt(lambda, N) / cln::expt(cln::cl_I(N),m[0]) * subexpr;
// 			N++;

// 		} while ( (res != resbuf) || flag_accidental_zero );
// 	}
// 	else {
// 		// N_trunc > 0, sum up the first N_trunc terms
// 		for (int N=1; N<=N_trunc; N++) {
// 			if ( depth > 1 ) {
// 				subexpr = 0;
// 				multi_iterator_ordered_eq<int> i_multi(1,N+1,depth-1);
// 				for( i_multi.init(); !i_multi.overflow(); i_multi++) {
// 					cln::cl_N tt = one;
// 					for (int j=1; j<depth; j++) {
// 						if ( j==1 ) {
// 							tt = tt * kernel[0]->series_coeff(N-i_multi[depth-2]) / cln::expt(cln::cl_I(i_multi[depth-2]),m[1]);
// 						}
// 						else {
// 							tt = tt * kernel[j-1]->series_coeff(i_multi[depth-j]-i_multi[depth-j-1]) / cln::expt(cln::cl_I(i_multi[depth-j-1]),m[j]);
// 						}
// 					}
// 					tt = tt * kernel[depth-1]->series_coeff(i_multi[0]);
// 					subexpr += tt;
// 				}
// 			}
// 			else {
// 				// depth == 1
// 				subexpr = kernel[0]->series_coeff(N) * one;
// 			}
// 			res += cln::expt(lambda, N) / cln::expt(cln::cl_I(N),m[0]) * subexpr;
// 		}
// 	}

// 	return res;
// }

// // figure out the number of basic_log_kernels before a non-basic_log_kernel
// cln::cl_N iterated_integral_prepare_m_lst(const std::vector<const integration_kernel *> & kernel_in, const cln::cl_N & lambda, int N_trunc)
// {
// 	size_t depth = kernel_in.size();

// 	std::vector<int> m;
// 	m.reserve(depth);

// 	std::vector<const integration_kernel *> kernel;
// 	kernel.reserve(depth);

// 	int n = 1;

// 	for (const auto & it : kernel_in) {
// 		if ( is_a<basic_log_kernel>(*it) ) {
// 			n++;
// 		}
// 		else {
// 			m.push_back(n);
// 			kernel.push_back( &ex_to<integration_kernel>(*it) );
// 			n = 1;
// 		}
// 	}

// 	cln::cl_N result = iterated_integral_do_sum(m, kernel, lambda, N_trunc);

// 	return result;
// }

// // shuffle to remove trailing zeros,
// // integration kernels, which are not basic_log_kernels, are treated as regularised kernels
// cln::cl_N iterated_integral_shuffle(const std::vector<const integration_kernel *> & kernel, const cln::cl_N & lambda, int N_trunc)
// {
//         cln::cl_F one = cln::cl_float(1, cln::float_format(Digits));

// 	const size_t depth = kernel.size();

// 	size_t i_trailing = 0;
// 	for (size_t i=0; i<depth; i++) {
// 		if ( !(is_a<basic_log_kernel>(*(kernel[i]))) ) {
// 			i_trailing = i+1;
// 		}
// 	}

// 	if ( i_trailing == 0 ) {
// 		return cln::expt(cln::log(lambda), depth) / cln::factorial(depth) * one;
// 	}

// 	if ( i_trailing == depth ) {
// 		return iterated_integral_prepare_m_lst(kernel, lambda, N_trunc);
// 	}

// 	// shuffle
// 	std::vector<const integration_kernel *> a,b;
// 	for (size_t i=0; i<i_trailing; i++) {
// 		a.push_back(kernel[i]);
// 	}
// 	for (size_t i=i_trailing; i<depth; i++) {
// 		b.push_back(kernel[i]);
// 	}

// 	cln::cl_N result = iterated_integral_prepare_m_lst(a, lambda, N_trunc) * cln::expt(cln::log(lambda), depth-i_trailing) / cln::factorial(depth-i_trailing);
// 	multi_iterator_shuffle_prime<const integration_kernel *> i_shuffle(a,b);
// 	for( i_shuffle.init(); !i_shuffle.overflow(); i_shuffle++) {
// 		std::vector<const integration_kernel *> new_kernel;
// 		new_kernel.reserve(depth);
// 		for (size_t i=0; i<depth; i++) {
// 			new_kernel.push_back(i_shuffle[i]);
// 		}

// 		result -= iterated_integral_shuffle(new_kernel, lambda, N_trunc);
// 	}

// 	return result;
// }

// } // end of anonymous namespace

// //////////////////////////////////////////////////////////////////////
// //
// // Iterated integrals
// //
// // GiNaC function
// //
// //////////////////////////////////////////////////////////////////////

// static ex iterated_integral_evalf_impl(const ex& kernel_lst, const ex& lambda, const ex& N_trunc)
// {
//         // sanity check
// 	if ((!kernel_lst.info(info_flags::list)) || (!lambda.evalf().info(info_flags::numeric)) || (!N_trunc.info(info_flags::nonnegint))) {
// 		return iterated_integral(kernel_lst,lambda,N_trunc).hold();
// 	}

//         lst k_lst = ex_to<lst>(kernel_lst);

// 	bool flag_not_numeric = false;
// 	for (const auto & it : k_lst) {
// 		if ( !is_a<integration_kernel>(it) ) {
// 			flag_not_numeric = true;
// 		}
// 	}
// 	if ( flag_not_numeric) {
// 		return iterated_integral(kernel_lst,lambda,N_trunc).hold();
// 	}

// 	for (const auto & it : k_lst) {
// 		if ( !(ex_to<integration_kernel>(it).is_numeric()) ) {
// 			flag_not_numeric = true;
// 		}
// 	}
// 	if ( flag_not_numeric) {
// 		return iterated_integral(kernel_lst,lambda,N_trunc).hold();
// 	}

// 	// now we know that iterated_integral gives a number

// 	int N_trunc_int = ex_to<numeric>(N_trunc).to_int();

// 	// check trailing zeros
// 	const size_t depth = k_lst.nops();

// 	std::vector<const integration_kernel *> kernel_vec;
// 	kernel_vec.reserve(depth);

// 	for (const auto & it : k_lst) {
// 		kernel_vec.push_back( &ex_to<integration_kernel>(it) );
// 	}

// 	size_t i_trailing = 0;
// 	for (size_t i=0; i<depth; i++) {
// 		if ( !(kernel_vec[i]->has_trailing_zero()) ) {
// 			i_trailing = i+1;
// 		}
// 	}

// 	// split integral into regularised integrals and trailing basic_log_kernels
// 	// non-basic_log_kernels are treated as regularised kernels in call to iterated_integral_shuffle
// 	cln::cl_F one = cln::cl_float(1, cln::float_format(Digits));
// 	cln::cl_N lambda_cln = ex_to<numeric>(lambda.evalf()).to_cl_N();
// 	basic_log_kernel L0 = basic_log_kernel();

// 	cln::cl_N result;
// 	if ( is_a<basic_log_kernel>(*(kernel_vec[depth-1])) ) {
// 		result = 0;
// 	}
// 	else {
// 		result = iterated_integral_shuffle(kernel_vec, lambda_cln, N_trunc_int);
// 	}

// 	cln::cl_N coeff = one;
// 	for (size_t i_plus=depth; i_plus>i_trailing; i_plus--) {
// 		coeff = coeff * kernel_vec[i_plus-1]->series_coeff(0);
// 		kernel_vec[i_plus-1] = &L0;
// 		if ( i_plus==i_trailing+1 ) {
// 			result += coeff * iterated_integral_shuffle(kernel_vec, lambda_cln, N_trunc_int);
// 		}
// 		else {
// 			if ( !(is_a<basic_log_kernel>(*(kernel_vec[i_plus-2]))) ) {
// 				result += coeff * iterated_integral_shuffle(kernel_vec, lambda_cln, N_trunc_int);
// 			}
// 		}
// 	}

// 	return numeric(result);
// }

// static ex iterated_integral2_evalf(const ex& kernel_lst, const ex& lambda)
// {
// 	return iterated_integral_evalf_impl(kernel_lst,lambda,0);
// }

// static ex iterated_integral3_evalf(const ex& kernel_lst, const ex& lambda, const ex& N_trunc)
// {
// 	return iterated_integral_evalf_impl(kernel_lst,lambda,N_trunc);
// }

// static ex iterated_integral2_eval(const ex& kernel_lst, const ex& lambda)
// {
// 	if ( lambda.info(info_flags::numeric) && !lambda.info(info_flags::crational) ) {
// 		return iterated_integral(kernel_lst,lambda).evalf();
// 	}

// 	return iterated_integral(kernel_lst,lambda).hold();
// }

// static ex iterated_integral3_eval(const ex& kernel_lst, const ex& lambda, const ex& N_trunc)
// {
// 	if ( lambda.info(info_flags::numeric) && !lambda.info(info_flags::crational) ) {
// 		return iterated_integral(kernel_lst,lambda,N_trunc).evalf();
// 	}

// 	return iterated_integral(kernel_lst,lambda,N_trunc).hold();
// }

// unsigned iterated_integral2_SERIAL::serial =
// 	function::register_new(function_options("iterated_integral", 2).
// 	                       eval_func(iterated_integral2_eval).
// 	                       evalf_func(iterated_integral2_evalf).
//                                do_not_evalf_params().
// 	                       overloaded(2));

// unsigned iterated_integral3_SERIAL::serial =
// 	function::register_new(function_options("iterated_integral", 3).
// 	                       eval_func(iterated_integral3_eval).
// 	                       evalf_func(iterated_integral3_evalf).
//                                do_not_evalf_params().
// 	                       overloaded(2));

} // namespace GiNaC

