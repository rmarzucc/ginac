/** @file sGt.cpp
 *
 *  Implementation of GiNaC's symbolic pathintegral. */

/*
 *  GiNaC Copyright (C) 1999-2020 Johannes Gutenberg University Mainz, Germany
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "sGt.h"
#include "numeric.h"
#include "symbol.h"
#include "add.h"
#include "mul.h"
#include "power.h"
#include "inifcns.h"
#include "wildcard.h"
#include "archive.h"
#include "registrar.h"
#include "utils.h"
#include "operators.h"
#include "relational.h"

using namespace std;

namespace GiNaC {

GINAC_IMPLEMENT_REGISTERED_CLASS_OPT(sGt, basic,
  print_func<print_dflt>(&sGt::do_print).
  print_func<print_python>(&sGt::do_print).
  print_func<print_latex>(&sGt::do_print_latex))


//////////
// default constructor
//////////

sGt::sGt()    //Not sure what exactly we should do here
		: tau(dynallocate<symbol>())
{}

//////////
// other constructors
//////////

// public

sGt::sGt(const ex & args_, const ex & z_, const ex & tau_)
		:  args(args_), z(z_), tau(tau_)
{
	if (!is_a<symbol>(tau_)) {
		throw(std::invalid_argument("last argument of sGt must be of type symbol"));
	}
	if (!is_a<lst>(args_)) {
		throw(std::invalid_argument("first argument must be a list"));
	}
}

//////////
// archiving
//////////

void sGt::read_archive(const archive_node& n, lst& sym_lst)
{
	inherited::read_archive(n, sym_lst);
	n.find_ex("args", args, sym_lst);
	n.find_ex("z", z, sym_lst);
	n.find_ex("tau", tau, sym_lst);
}

void sGt::archive(archive_node & n) const
{
	inherited::archive(n);
	n.add_ex("args", args);
	n.add_ex("z", z);
	n.add_ex("tau", tau);
}

//////////
// functions overriding virtual functions from base classes
//////////

void sGt::do_print(const print_context & c, unsigned level) const
{
	c.s << "sGt(";
	args.print(c);
	c.s << ",";
	z.print(c);
	c.s << ",";
	tau.print(c);
	c.s << ")";
}

void sGt::do_print_latex(const print_latex & c, unsigned level) const //Still need to implement this properly
{
	string varname = ex_to<symbol>(args).get_name();
	c.s << "\\tilde_{\\Gamma}\\left(";
	args.print(c);
	c.s << ",";
	z.print(c);
	c.s << ";";
	tau.print(c);
	c.s << "\\right)";
}

int sGt::compare_same_type(const basic & other) const
{
	GINAC_ASSERT(is_exactly_a<sGt>(other));
	const sGt &o = static_cast<const sGt &>(other);

	int cmpval = args.compare(o.args);
	if (cmpval)
		return cmpval;
	cmpval = z.compare(o.z);
	if (cmpval)
		return cmpval;
	return tau.compare(o.tau);
}

ex sGt::eval() const
{
	if (flags & status_flags::evaluated)
		return *this;

		//Maybe replace sGt[{{0,0}},z;tau] -> z

	// if (!f.has(x) && !haswild(f))
	// 	return x*f-b.op(0)*f;

	//if (b.op(0)==b.op(1))
	//	return _ex0;

	return this->hold();
}

// ex sGt::evalf() const
// {
// 	lst repL = lst{z1,z2,z3,z,tt};
// 	lst repR = lst{1/6 + 1/6*I, 1/4 + 1/4*I, 1/2 + 1/2*I, 1/2, I};
// 	ex expr = sGt(args,z,tau);

// 	return expr.subs(repL,repR);

// }


ex sGt::evalf() const
{
	ex eargs = args.evalf();
	ex ez = z.evalf();

	// If all entries are numeric, evaluate sGt?

	if (are_ex_trivially_equal(args, eargs) &&
	    are_ex_trivially_equal(z, ez))
			return *this;
		else
			return dynallocate<sGt>(eargs, ez, tau);
}

// ex psubsvalue(const ex & var, const ex & value, const ex & fun)
// {
// 	ex result = fun.subs(var==value).evalf();
// 	if (is_a<numeric>(result))
// 		return result;
// 	throw logic_error("integrand does not evaluate to numeric");
// }

// int sGt::degree(const ex & s) const
// {
// 	return ((b.op(1)-b.op(0))*f).degree(s);
// }

// int sGt::ldegree(const ex & s) const
// {
// 	return ((b.op(1)-b.op(0))*f).ldegree(s);
// }

// ex sGt::eval_ncmul(const exvector & v) const
// {
// 	return f.eval_ncmul(v);
// }

size_t sGt::nops() const
{
	return 3;
}

ex sGt::op(size_t i) const
{
	GINAC_ASSERT(i<3);

	switch (i) {
		case 0:
			return args;
		case 1:
			return z;
		case 2:
			return tau;
		default:
			throw (std::out_of_range("sGt::op() out of range"));
	}
}

ex & sGt::let_op(size_t i)
{
	ensure_if_modifiable();
	switch (i) {
		case 0:
			return args;
		case 1:
			return z;
		case 2:
			return tau;
		default:
			throw (std::out_of_range("sGt::let_op() out of range"));
	}
}



bool realSort (ex i,ex j, ex points) 
{ 
	return ( real(ex_to<numeric>(i.subs(points))) < real(ex_to<numeric>(j.subs(points))) ); 
}

ex sgtWexpand (size_t n, ex w, ex x, ex qtau, size_t ord) 
{
	if (n == 0)
	{
		return -I*pow(2*Pi,-1)*pow(w,-1);
	} else
	{
		ex hlp = 0;
		if (n == 1)
		{
			hlp += (I*pow(2*Pi,-1)*pow(w-qq(1,x),-1) - I*pow(2*Pi,-1)*pow(w,-1))*sgtWCoeff(n,x,qtau,I,ord);
		}

		for (size_t i = 1; i <= ord; i++)
		{
			hlp -= I*pow(2*Pi,-1)*pow(w,i-1)*sgtWCoeff(n,x,qtau,i,ord);
		}

		ex indexhlp;
		for (size_t i = 0; i <= ord; i++)
		{
			indexhlp = i;
			hlp -= I*pow(2*Pi,-1)*pow(w,-indexhlp-1)*sgtWCoeff(n,x,qtau,-indexhlp,ord);
		}

		return hlp; 
		
	}
	
}

// ex sgtWexpand (size_t n, ex w, ex x, ex tau, size_t ord) 
// {
// 	if (n == 0)
// 	{
// 		return -I*pow(2*Pi,-1)*pow(w,-1);
// 	} else
// 	{
// 		ex hlp = 0;
// 		if (n == 1)
// 		{
// 			hlp += (I*pow(2*Pi,-1)*pow(w-qq(1,x),-1) - I*pow(2*Pi,-1)*pow(w,-1))*sgtWCoeff(n,x,tau,I,ord);
// 		}

// 		for (size_t i = 1; i <= ord; i++)
// 		{
// 			hlp -= I*pow(2*Pi,-1)*pow(w,i-1)*sgtWCoeff(n,x,tau,i,ord);
// 		}

// 		ex indexhlp;
// 		for (size_t i = 0; i <= ord; i++)
// 		{
// 			indexhlp = i;
// 			hlp -= I*pow(2*Pi,-1)*pow(w,-indexhlp-1)*sgtWCoeff(n,x,tau,-indexhlp,ord);
// 		}

// 		return hlp; 
		
// 	}
	
// }

ex sGt::Nqexpand(ex points, int ord, unsigned options) const
{
	exvector zis, nis, Nzis, Nnis, path;
	bool numericQ = true;
	ex Nz, Ntau;
	vector<size_t> critPos;
	size_t nargs = args.nops();

	zis.reserve(nargs);
	Nzis.reserve(nargs);
	nis.reserve(nargs);
	Nnis.reserve(nargs);
	critPos.reserve(nargs);
	path.reserve(nargs);

	for (size_t i = 0; i < nargs; i++)
	{
		nis.push_back(args.op(i).op(0));
		zis.push_back(args.op(i).op(1));
		Nzis.push_back(args.op(i).op(1).subs(points));
		Nnis.push_back(args.op(i).op(0).subs(points));
		numericQ = numericQ && is_a<numeric>(args.op(i).op(1).subs(points)) && is_integer(ex_to<numeric>(args.op(i).op(0)));
	}

	Nz = z.subs(points);
	Ntau = tau.subs(points);

	numericQ = numericQ && is_a<numeric>(Nz) && is_a<numeric>(Ntau);
	

	if (!numericQ) // I think the q-expansion should also work if all z's are rational points r1 + r2 tau
	{
		throw logic_error("All arguments of eMPLs must evaluate to numbers and all ni must evaluate to integers for the given points.");
	}

	numeric Numz = ex_to<numeric>(Nz);
	numeric Numtau = ex_to<numeric>(Ntau);

	// for (std::vector<ex>::iterator it = Nnis.begin() ; it != Nnis.end(); ++it)
    // {
	// 	if (ex_to<numeric>(it) == 1)
	// 	{
	// 		/* code */
	// 	}
	// }

	if (real(Numz) > 0)
	{
		numeric zi;

		for (size_t i = 0; i < Nnis.size(); i++)
		{
			if (ex_to<numeric>(Nnis[i])==1) 
			{
				zi = ex_to<numeric>(Nzis[i]);

				if (real(Numz) > real(zi) && real(zi) > 0 && imag(zi) > real(zi)*inverse(real(Numz))*imag(Numz))
				{
					critPos.push_back(i);
					path.push_back(real(zi)); // Integration over numbers or symbols preferable? (Issue with pintegrate? normalisation of denominators with floats?)
				}
			}
			
		}

		std::sort (path.begin(), path.end(), [points](ex i, ex j){ return i<j; });
		path.erase( unique( path.begin(), path.end() ), path.end() );
		//std::sort (path.begin(), path.end(), [points](ex i, ex j){ return realSort(i,j,points); });
	}

	if (real(Numz) < 0)
	{
		numeric zi;

		for (size_t i = 0; i < Nnis.size(); i++)
		{
			if (Nnis[i]==1) // Already check direction of integration here
			{
				zi = ex_to<numeric>(Nzis[i]);
				if (real(Numz) < real(zi) && real(zi) < 0 && imag(zi) > real(zi)*inverse(real(Numz))*imag(Numz))
				{
					critPos.push_back(i);
					path.push_back(real(zi)); // Integration over numbers or symbols preferable? (Issue with pintegrate? normalisation of denominators with floats?)
				}
			}
			
		}

		std::sort (path.begin(), path.end(), [points](ex i, ex j){ return j<i; });
		path.erase( unique( path.begin(), path.end() ), path.end() );
		//std::sort (path.begin(), path.end(), [points](ex i, ex j){ return realSort(i,j,points); });
	}

	lst wpath;
	wpath.append(1);

	for (size_t i = 0; i < path.size(); ++i)
    {
		wpath.append(qq(1,Numz*path[i]*pow(real(Numz),-1)));
	}

	wpath.append(qq(1,Nz));
	//cout << "path = " << wpath << endl;

	ex inthlp = 1;
	ex kerexpansion;
	symbol w("w");
	symbol qtau("qtau");

	for (size_t i = 0; i < nargs; i++) //Integrate last kernel first!
	{
		kerexpansion = sgtWexpand(ex_to<numeric>(Nnis[nargs - 1 - i]).to_long(),w,zis[nargs - 1 - i],qtau,ord);
		inthlp = pintegral(w,wpath,inthlp * kerexpansion).eval_integ();
	}

	//return inthlp.subs(w==qq(1,z));

	inthlp = inthlp.subs(w==qq(1,z)).evalf().series(qtau,ord+1).subs(qtau==qq(1,tau));

	return inthlp.subs(points).evalf();
	

	// lst out;

	// for (size_t i = 0; i < path.size(); ++i)
    // {
	// 	out.append(wpath[i]);
	// }

	// return out;
	
}


ex sGt::derivative(const symbol & s) const
{
	if (s==z) 
	{
		lst args2 = ex_to<lst>(args);
		return ssgt(args.op(0).op(0),z-args.op(0).op(1),tau)*sGt(args2.remove_first(), z, tau);
	} else
	{
		throw logic_error("Only derivations with respect to z implemented");
	}
	
}

// unsigned sGt::return_type() const
// {
// 	return tau.return_type();
// }

// return_type_t sGt::return_type_tinfo() const
// {
// 	return tau.return_type_tinfo();
// }

ex sGt::conjugate() const
{
	lst args2 = ex_to<lst>(args);
	lst newargs;
	size_t len = args2.nops();
	ex n;
	ex zi;
	for (size_t i = 0; i < len; i++)
	{
		n = args.op(i).op(0);
		zi = args.op(i).op(1).conjugate();
		newargs.append(lst({n,zi}));
	}
	
	ex conjargs = lst(newargs);
	ex conjz = z.conjugate();

	if (are_ex_trivially_equal(args, conjargs) &&
	    are_ex_trivially_equal(z, conjz))
		return *this;

	return dynallocate<sGt>(conjargs, conjz, tau);
}



GINAC_BIND_UNARCHIVER(sGt);
} // namespace GiNaC
