/** @file pintegral.cpp
 *
 *  Implementation of GiNaC's symbolic pathintegral. */

/*
 *  GiNaC Copyright (C) 1999-2020 Johannes Gutenberg University Mainz, Germany
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "pintegral.h"
#include "GPath.h"
#include "numeric.h"
#include "symbol.h"
#include "add.h"
#include "mul.h"
#include "power.h"
#include "inifcns.h"
#include "wildcard.h"
#include "archive.h"
#include "registrar.h"
#include "utils.h"
#include "operators.h"
#include "relational.h"

using namespace std;

namespace GiNaC {

GINAC_IMPLEMENT_REGISTERED_CLASS_OPT(pintegral, basic,
  print_func<print_dflt>(&pintegral::do_print).
  print_func<print_python>(&pintegral::do_print).
  print_func<print_latex>(&pintegral::do_print_latex))


//////////
// default constructor
//////////

pintegral::pintegral()
		: x(dynallocate<symbol>())
{}

//////////
// other constructors
//////////

// public

pintegral::pintegral(const ex & x_, const ex & b_lst, const ex & f_)
		:  x(x_), b(b_lst), f(f_)
{
	if (!is_a<symbol>(x)) {
		throw(std::invalid_argument("first argument of pintegral must be of type symbol"));
	}
	if (!is_a<lst>(b_lst)) {
		throw(std::invalid_argument("second argument must be a list of points specifying the path of integration"));
	}
}

//////////
// archiving
//////////

void pintegral::read_archive(const archive_node& n, lst& sym_lst)
{
	inherited::read_archive(n, sym_lst);
	n.find_ex("x", x, sym_lst);
	n.find_ex("b", b, sym_lst);
	n.find_ex("f", f, sym_lst);
}

void pintegral::archive(archive_node & n) const
{
	inherited::archive(n);
	n.add_ex("x", x);
	n.add_ex("b", b);
	n.add_ex("f", f);
}

//////////
// functions overriding virtual functions from base classes
//////////

void pintegral::do_print(const print_context & c, unsigned level) const
{
	c.s << "pintegral(";
	x.print(c);
	c.s << ",";
	b.print(c);
	c.s << ",";
	f.print(c);
	c.s << ")";
}

void pintegral::do_print_latex(const print_latex & c, unsigned level) const
{
	string varname = ex_to<symbol>(x).get_name();
	if (level > precedence())
		c.s << "\\left(";
	c.s << "\\int_{";
	b.print(c);
	c.s << "}^{";
	b.print(c);
	c.s << "} d";
	if (varname.size() > 1)
		c.s << "\\," << varname << "\\:";
	else
		c.s << varname << "\\,";
	f.print(c,precedence());
	if (level > precedence())
		c.s << "\\right)";
}

int pintegral::compare_same_type(const basic & other) const
{
	GINAC_ASSERT(is_exactly_a<pintegral>(other));
	const pintegral &o = static_cast<const pintegral &>(other);

	int cmpval = x.compare(o.x);
	if (cmpval)
		return cmpval;
	cmpval = b.compare(o.b);
	if (cmpval)
		return cmpval;
	return f.compare(o.f);
}

ex pintegral::eval() const
{
	if (flags & status_flags::evaluated)
		return *this;

	// if (!f.has(x) && !haswild(f))
	// 	return x*f-b.op(0)*f;

	//if (b.op(0)==b.op(1))
	//	return _ex0;

	return this->hold();
}

ex pintegral::evalf() const
{
	ex eb = b.evalf();
	ex ef = f.evalf();

	// 12.34 is just an arbitrary number used to check whether a number
	// results after substituting a number for the integration variable.
	// if (is_exactly_a<numeric>(ea) && is_exactly_a<lst>(eb) &&
	//     is_exactly_a<numeric>(ef.subs(x==12.34).evalf())) {
	// 		return adaptivesimpson(x, ea, eb, ef);
	// }

	if (are_ex_trivially_equal(b, eb) &&
	    are_ex_trivially_equal(f, ef))
			return *this;
		else
			return dynallocate<pintegral>(x, eb, ef);
}

ex psubsvalue(const ex & var, const ex & value, const ex & fun)
{
	ex result = fun.subs(var==value).evalf();
	if (is_a<numeric>(result))
		return result;
	throw logic_error("integrand does not evaluate to numeric");
}

ex pfCoeff(const int & k, const int n, const ex & a, const int m, const ex & b, const int & e)
{
	ex hlp = 0;
	for (int s = std::max(0,e - n + 1); s <= std::min(k,e) ; s++)
	{
		hlp += pow(-1,e+s)*binomial(m-1+e-s,m-1)*binomial(k,s)*pow(a-b,s)*pow(a,k-s);
	}
	return hlp;
}

// pfCoeff[k_, {n_, a_}, {m_, b_}, e_] := 
//  Module[{lb = Max[0, e - n + 1], ub = Min[k, e], hlp},
//   hlp = Sum[(-1)^(e + s)*
//      Binomial[m - 1 + e - s, m - 1] Binomial[k, s] (a - b)^s*
//      a^(k - s), {s, lb, ub}];
//   Return[hlp]
//   ]

// typedef map<error_and_integral, ex, error_and_integral_is_less> lookup_map;

// // Maybe implement path decomposition into integrals and implement adaptive simpson for those

// /** Numeric integration routine based upon the "Adaptive Quadrature" one
//   * in "Numerical Analysis" by Burden and Faires. Parameters are integration
//   * variable, left boundary, right boundary, function to be integrated and
//   * the relative integration error. The function should evalf into a number
//   * after substituting the integration variable by a number. Another thing
//   * to note is that this implementation is no good at integrating functions
//   * with discontinuities. */
// ex adaptivesimpson(const ex & x, const ex & a_in, const ex & b_in, const ex & f, const ex & error)
// {
// 	// Check whether boundaries and error are numbers.
// 	ex a = is_exactly_a<numeric>(a_in) ? a_in : a_in.evalf();
// 	ex b = is_exactly_a<numeric>(b_in) ? b_in : b_in.evalf();
// 	if(!is_exactly_a<numeric>(a) || !is_exactly_a<numeric>(b))
// 		throw std::runtime_error("For numerical integration the boundaries of the integral should evalf into numbers.");
// 	if(!is_exactly_a<numeric>(error))
// 		throw std::runtime_error("For numerical integration the error should be a number.");

// 	// Use lookup table to be potentially much faster.
// 	static lookup_map lookup;
// 	static symbol ivar("ivar");
// 	ex lookupex = pintegral(ivar,a,b,f.subs(x==ivar));
// 	auto emi = lookup.find(error_and_integral(error, lookupex));
// 	if (emi!=lookup.end())
// 		return emi->second;

// 	ex app = 0;
// 	int i = 1;
// 	exvector avec(pintegral::max_integration_level+1);
// 	exvector hvec(pintegral::max_integration_level+1);
// 	exvector favec(pintegral::max_integration_level+1);
// 	exvector fbvec(pintegral::max_integration_level+1);
// 	exvector fcvec(pintegral::max_integration_level+1);
// 	exvector svec(pintegral::max_integration_level+1);
// 	exvector errorvec(pintegral::max_integration_level+1);
// 	vector<int> lvec(pintegral::max_integration_level+1);

// 	avec[i] = a;
// 	hvec[i] = (b-a)/2;
// 	favec[i] = subsvalue(x, a, f);
// 	fcvec[i] = subsvalue(x, a+hvec[i], f);
// 	fbvec[i] = subsvalue(x, b, f);
// 	svec[i] = hvec[i]*(favec[i]+4*fcvec[i]+fbvec[i])/3;
// 	lvec[i] = 1;
// 	errorvec[i] = error*abs(svec[i]);

// 	while (i>0) {
// 		ex fd = subsvalue(x, avec[i]+hvec[i]/2, f);
// 		ex fe = subsvalue(x, avec[i]+3*hvec[i]/2, f);
// 		ex s1 = hvec[i]*(favec[i]+4*fd+fcvec[i])/6;
// 		ex s2 = hvec[i]*(fcvec[i]+4*fe+fbvec[i])/6;
// 		ex nu1 = avec[i];
// 		ex nu2 = favec[i];
// 		ex nu3 = fcvec[i];
// 		ex nu4 = fbvec[i];
// 		ex nu5 = hvec[i];
// 		// hopefully prevents a crash if the function is zero sometimes.
// 		ex nu6 = max(errorvec[i], abs(s1+s2)*error);
// 		ex nu7 = svec[i];
// 		int nu8 = lvec[i];
// 		--i;
// 		if (abs(ex_to<numeric>(s1+s2-nu7)) <= nu6)
// 			app+=(s1+s2);
// 		else {
// 			if (nu8>=integral::max_integration_level)
// 				throw runtime_error("max integration level reached");
// 			++i;
// 			avec[i] = nu1+nu5;
// 			favec[i] = nu3;
// 			fcvec[i] = fe;
// 			fbvec[i] = nu4;
// 			hvec[i] = nu5/2;
// 			errorvec[i]=nu6/2;
// 			svec[i] = s2;
// 			lvec[i] = nu8+1;
// 			++i;
// 			avec[i] = nu1;
// 			favec[i] = nu2;
// 			fcvec[i] = fd;
// 			fbvec[i] = nu3;
// 			hvec[i] = hvec[i-1];
// 			errorvec[i]=errorvec[i-1];
// 			svec[i] = s1;
// 			lvec[i] = lvec[i-1];
// 		}
// 	}

// 	lookup[error_and_integral(error, lookupex)]=app;
// 	return app;
// }

int pintegral::degree(const ex & s) const
{
	return ((b.op(1)-b.op(0))*f).degree(s);
}

int pintegral::ldegree(const ex & s) const
{
	return ((b.op(1)-b.op(0))*f).ldegree(s);
}

ex pintegral::eval_ncmul(const exvector & v) const
{
	return f.eval_ncmul(v);
}

size_t pintegral::nops() const
{
	return 3;
}

ex pintegral::op(size_t i) const
{
	GINAC_ASSERT(i<3);

	switch (i) {
		case 0:
			return x;
		case 1:
			return b;
		case 2:
			return f;
		default:
			throw (std::out_of_range("pintegral::op() out of range"));
	}
}

ex & pintegral::let_op(size_t i)
{
	ensure_if_modifiable();
	switch (i) {
		case 0:
			return x;
		case 1:
			return b;
		case 2:
			return f;
		default:
			throw (std::out_of_range("pintegral::let_op() out of range"));
	}
}

ex pintegral::expand(unsigned options) const
{
	if (options==0 && (flags & status_flags::expanded))
		return *this;

	ex newb = b.expand(options);
	ex newf = f.expand(options);

	if (is_a<add>(newf)) {
		exvector v;
		v.reserve(newf.nops());
		for (size_t i=0; i<newf.nops(); ++i)
			v.push_back(pintegral(x, newb, newf.op(i)).expand(options));
		return ex(add(v)).expand(options);
	}

	if (is_a<mul>(newf)) {
		ex prefactor = 1;
		ex rest = 1;
		for (size_t i=0; i<newf.nops(); ++i)
			if (newf.op(i).has(x))
				rest *= newf.op(i);
			else
				prefactor *= newf.op(i);
		if (prefactor != 1)
			return (prefactor*pintegral(x, newb, rest)).expand(options);
	}

	if (are_ex_trivially_equal(b, newb) &&
	    are_ex_trivially_equal(f, newf)) {
		if (options==0)
			this->setflag(status_flags::expanded);
		return *this;
	}

	const pintegral & newint = dynallocate<pintegral>(x, newb, newf);
	if (options == 0)
		newint.setflag(status_flags::expanded);
	return newint;
}

// Minimal partial fractioning algorithm that suffices in our case. If there are faster, more general 
// algorithms around I'll be happy to implement one of those.

ex pintegral::apart(unsigned options) const
{

	if (options==0 && (flags & status_flags::pfed))
		return *this;

	ex newf = f.expand(options);

	if (is_a<add>(newf)) {
		exvector v;
		v.reserve(newf.nops());
		for (size_t i=0; i<newf.nops(); ++i)
			v.push_back(pintegral(x, b, newf.op(i)).apart(options));
		return ex(add(v));
	}

	if (is_a<mul>(newf)) {
		ex prefactor = 1;
		ex rest = 1;
		for (size_t i=0; i<newf.nops(); ++i)
			if (newf.op(i).has(x))
				rest *= newf.op(i);
			else
				prefactor *= newf.op(i);
		if (prefactor != 1)
			return (prefactor*pintegral(x, b, rest).apart(options));
	}

	// Sorting the expression. All factors free of x should have already been pulled out.

	if (is_a<mul>(newf)) 
	{
		int xpow = 0;
		ex prefactor = 1;
		ex trans = 1;
		exvector offsets;
		offsets.reserve(newf.nops());
		std::vector<int> exps;
		exps.reserve(newf.nops());
		for (size_t i=0; i<newf.nops(); ++i)
		{
			if (newf.op(i) == x)
				xpow = 1;
			else if (is_a<GPath>(newf.op(i))) // Need to distinguish between G's depending on x or not
			{
				trans *= newf.op(i);
			} else if (is_a<power>(newf.op(i)))
			{
				//den.push_back(newf.op(i));
				if (newf.op(i).op(0) == x)
				{
					xpow = ex_to<numeric>(newf.op(i).op(1)).to_int();
				}
				else
				{
					ex hlp = newf.op(i);
					exps.push_back(ex_to<numeric>(-hlp.op(1)).to_int());
					ex off = hlp.op(0).subs(x==0);
					ex pref = (hlp.op(0)-off).subs(x==1); //We assume all denominators to be powers of linear functions in x

					if (pref != 1)
					{
						prefactor *= pow(pref,hlp.op(1));
						offsets.push_back(-off*pow(pref,-1));
					}
					else
					{	
						offsets.push_back(-off); // Define the offset of x-a to be a, not -a
					}

				}
			
			}
		}

		if (exps.size() == 1)
		{
			if (xpow > 0)
			{
				ex tmp = 0;
				for (int i = 0; i <= xpow; i++)
				{
					tmp += binomial(xpow,i) * pow(offsets[0],(xpow - i)) * pintegral(x,b,trans*pow(x-offsets[0],-exps[0]+i));
				}

				return prefactor * tmp;

			} else if (xpow < 0)
			{
				ex hlp = 0;
				for (int i = 0; i < -xpow; i++)
				{
					hlp += pow(-1,i)*binomial(exps[0]+i-1,i)*pow(-offsets[0],-exps[0]-i) 
							* pintegral(x,b,pow(x,xpow+i)*trans);
				}
				
				for (int i = 0; i < exps[0]; i++)
				{
					hlp += pow(-1,i)*binomial(-xpow+i-1,i)*pow(offsets[0],xpow-i)
							* pintegral(x,b,pow(x-offsets[0],-exps[0]+i)*trans);
				}

				return prefactor*hlp;

			} 
			// else if (xpow == 0)
			// {
			// 	if (prefactor != 1)
			// 	{
			// 		return prefactor * pintegral(x,b,pow(x-offsets[0],-exps[0])*trans);
			// 	} //else return nothing and finally reach newf = f -> set flag pfed;

			// }
			
			

			
		} else if (exps.size() == 2)
		{
			ex hlp = 0;
			ex aa = offsets[0];
			ex bb = offsets[1];
			int nn = exps[0];
			int mm = exps[1];
			if (xpow == 0)
			{
				for (int i = 0; i < exps[0]; i++)
				{
					hlp += pow(-1,i)*binomial(exps[1]+i-1,i)*pow(offsets[0]-offsets[1],-exps[1]-i) 
							* pintegral(x,b,pow(x-offsets[0],-exps[0]+i)*trans);
				}
				
				for (int i = 0; i < exps[1]; i++)
				{
					hlp += pow(-1,i)*binomial(exps[0]+i-1,i)*pow(offsets[1]-offsets[0],-exps[0]-i)
							* pintegral(x,b,pow(x-offsets[1],-exps[1]+i)*trans);
				}

				return prefactor*hlp;
			} else if (xpow > 0)
			{
				for (int j = 0; j <= xpow + nn - 1; j++)
				{
					hlp += pfCoeff(xpow,nn,aa,mm,bb,j)*pow(aa-bb,-mm-j)* pintegral(x,b,trans*pow(x-aa,-nn+j));
				}
				
				for (int j = 0; j <= xpow + mm - 1; j++)
				{
					hlp += pfCoeff(xpow,mm,bb,nn,aa,j)*pow(bb-aa,-nn-j)* pintegral(x,b,pow(x-bb,-mm+j)*trans);
				}
				return prefactor * hlp;
			} else if (xpow < 0)
			{
				cout << "Unexpected mumber of denominators (1/x) \n";
				return *this;
			}
			

		} else if (exps.size() > 2)
		{
			cout << "Unexpected mumber of denominators (more than 2 denominators with offsets) \n";
			return *this;
		}

	}

	if (are_ex_trivially_equal(f, newf)) {
		if (options==0)
			this->setflag(status_flags::pfed);
		return *this;
	}

	const pintegral & newint = dynallocate<pintegral>(x, b, newf);
	if (options == 0)
		newint.setflag(status_flags::pfed);
	return newint;
}

ex pintegral::derivative(const symbol & s) const
{
	if (s==x)
		throw(logic_error("differentiation with respect to dummy variable"));
	return b.op(1).diff(s)*f.subs(x==b.op(1))-b.op(0).diff(s)*f.subs(x==b.op(0))+pintegral(x, b, f.diff(s));
}

unsigned pintegral::return_type() const
{
	return f.return_type();
}

return_type_t pintegral::return_type_tinfo() const
{
	return f.return_type_tinfo();
}

ex pintegral::conjugate() const
{
	ex conjb = b.conjugate();
	ex conjf = f.conjugate().subs(x.conjugate()==x);

	if (are_ex_trivially_equal(b, conjb) &&
	    are_ex_trivially_equal(f, conjf))
		return *this;

	return dynallocate<pintegral>(x, conjb, conjf);
}

ex pintegral::eval_integ() const
{
	

	if (!(flags & status_flags::pfed))
	{
		return this->apart().eval_integ();
	}

	if (!f.has(x))
	{
		return f*x-f*b.op(0);
	}

	if (f == x)
	{
		return pow(x,2)/2 - pow(b.op(0),2)/2;
	}
	

	if (is_a<power>(f) && f.op(0) == x)
	{	
		ex n = f.op(1);
		if (!n.has(x))
		{
			if (n == -1)
			{
				return GPath(lst{0},x,b);
			} else
			{
				return pow(x,n+1)*pow(n+1,-1) - pow(b.op(0),n+1)*pow(n+1,-1);
			}

		} else
		{
			throw logic_error("Exponent depends on integration variable");
			return *this;
		}
		
		
	}

	if (is_a<power>(f) && !f.op(1).has(x))
	{	
		ex n = f.op(1);
		ex off = -f.op(0).subs(x==0);
		ex pref = (f.op(0)+off).subs(x==1); //We assume all denominators to be powers of linear functions in x
		if (pref != 1)
		{
			off = off*pow(pref,-1);
		}
		if (n != -1)
		{
			return pow(pref,n) * pow(x-off,n+1)*pow(n+1,-1) - pow(pref,n) * pow(b.op(0)-off,n+1)*pow(n+1,-1);
		} else if (n == -1)
		{
			return pow(pref,-1)*GPath(lst{off},x,b);
		}
		
		
	}

	if (is_a<GPath>(f))
	{
		lst args = ex_to<lst>(f.op(0));
		ex aa = args.op(0);
		if (args.nops() == 1)
		{
			return x*GPath(args,f.op(1),f.op(2)) - pintegral(x,b,x*pow(x-aa,-1)).eval_integ();
			//GPathint[GPath[u_, t_,x_], t_, x_] := t*GPath[u, t, x] - GPathint[Expand[Apart[(t)/(t - u), t]], t, x]
		} else
		{
			return x*GPath(args,f.op(1),f.op(2)) - pintegral(x,b,x*pow(x-aa,-1)*GPath(args.remove_first(),f.op(1),f.op(2))).eval_integ();
			//GPathint[GPath[u_, v__,t_,x_], t_, x_] := t*GPath[u, v,t, x] - GPathint[Expand[Apart[(t*GPath[v, t,x])/(t - u), t]], t, x]
		}
	}
	
	

	if (is_a<mul>(f)) 
	{
		int xpow = 0;
		ex prefactor = 1;
		ex trans = 1;
		exvector offsets;
		offsets.reserve(f.nops());
		std::vector<int> exps;
		exps.reserve(f.nops());
		for (size_t i=0; i<f.nops(); ++i)
		{
			if (f.op(i) == x)
				xpow = 1;
			else if (is_a<GPath>(f.op(i))) // Need to implement shuffles for generic path integrals
			{
				trans *= f.op(i);
			} else if (is_a<power>(f.op(i)))
			{
				//den.push_back(newf.op(i));
				if (f.op(i).op(0) == x)
				{
					xpow = ex_to<numeric>(f.op(i).op(1)).to_int();
				}
				else
				{
					ex hlp = f.op(i);
					exps.push_back(ex_to<numeric>(-hlp.op(1)).to_int());
					ex off = hlp.op(0).subs(x==0);
					ex pref = (hlp.op(0)-off).subs(x==1); //We assume all denominators to be powers of linear functions in x
					if (pref != 1)
					{
						prefactor *= pow(pref,hlp.op(1));
						offsets.push_back(-off*pow(pref,-1));
					}
					else
					{	
						offsets.push_back(-off); // Define the offset of x-a to be a, not -a
					}

				}
			
			}
		}


		if (exps.size() == 0) 
		{
			if (trans == 1)// should not happen if f is a product and exps = {} at this point
			{
				if (xpow > 0 || xpow < -1)
				{
					return prefactor * pow(x,xpow + 1)*pow(xpow + 1,-1) - prefactor * pow(b.op(0),xpow + 1)*pow(xpow + 1,-1);
				} else if (xpow == -1)
				{
					return prefactor * GPath(lst{0},x,b);
				} 
			} else if (is_a<GPath>(trans)) // Should not contain any GPath without x. Make sure paths of GPath and pintegral match!
			{
				if (xpow > 0 || xpow < -1)
				{
					if (is_a<lst>(trans.op(0)))
					{
						lst args = ex_to<lst>(trans.op(0));
						ex aa = args.op(0);
						if (args.nops() == 1)
						{
							return prefactor * pow(x,xpow+1)/(xpow+1)*GPath(args,trans.op(1),trans.op(2)) 
							- prefactor * pow(xpow+1,-1)*(pintegral(x,b,pow(x,xpow+1)*pow(x-aa,-1)).eval_integ());
							// GPathint[GPath[u_, t_,x_]*(t_)^(n_.), t_, x_] := (1*t^(n + 1)*GPath[u,t, x])/(n + 1) 
							// - GPathint[Expand[Apart[(1*t^(n + 1))/((n + 1)*(t - u)), t]], t, x] /; n != -1
						} else
						{
							return prefactor * pow(x,xpow+1)/(xpow+1)*GPath(args,trans.op(1),trans.op(2)) 
							- prefactor * pow(xpow+1,-1)*pintegral(x,b,pow(x,xpow+1)*pow(x-aa,-1)*GPath(args.remove_first(),trans.op(1),trans.op(2))).eval_integ();
							// GPathint[GPath[u_, v__, t_,x_]*(t_)^(n_.), t_, x_] := (1*t^(n + 1)*GPath[u, v,t, x])/(n + 1) 
							// - GPathint[Expand[Apart[(1*t^(n + 1)*GPath[v, t,x])/((n + 1)*(t - u)), t]], t, x] /; n != -1
						}

					}
				} else if (xpow == -1)
				{
					if (is_a<lst>(trans.op(0)))
					{
						lst args = ex_to<lst>(trans.op(0));
						return prefactor * GPath(args.prepend(0),trans.op(1),trans.op(2));
					}
				} 
			}
			
			
		}

		if (exps.size() == 1)
		{
			if (xpow != 0) // Should not happen if correctly partial fractioned
			{
				cout << "Expression not partial fractioned! \n";
				return *this;
			} else
			{
				if (trans == 1) // Should not happen
				{
					cout << "Flag bad mul 1: f is " << f <<" \n";
					return *this;
				} else if (is_a<GPath>(trans))
				{
					if (exps[0] > 1 || exps[0] < 0) // exponents are exponents of denominators. exps[0] < 0 should not occur.
					{
						if (is_a<lst>(trans.op(0)))
						{
							lst args = ex_to<lst>(trans.op(0));
							ex aa = args.op(0);
							ex off = offsets[0];
							if (args.nops() == 1)
							{
								return prefactor*pow(x-off,-exps[0]+1)*pow(-exps[0]+1,-1)*GPath(args,trans.op(1),trans.op(2)) 
								- prefactor*pow(-exps[0]+1,-1)*(pintegral(x,b,pow(x-off,-exps[0]+1)*pow(x-aa,-1)).eval_integ());
								//GPathint[GPath[u_, t_,x_]*((g_.) + (h_.)*(t_))^(n_.), t_, x_] := (1*(g + h*t)^(n + 1)*GPath[u,t, x])/((n + 1)*h)
     							// - GPathint[Expand[Apart[(1*(g + h*t)^(n + 1))/((n + 1)*h*(t - u)), t]], t, x] /; n != -1 && FreeQ[{g, h}, t]
							} else
							{
								return prefactor*pow(x-off,-exps[0]+1)*pow(-exps[0]+1,-1)*GPath(args,trans.op(1),trans.op(2)) 
								- prefactor*pow(-exps[0]+1,-1)*pintegral(x,b,pow(x-off,-exps[0]+1)*pow(x-aa,-1)*GPath(args.remove_first(),trans.op(1),trans.op(2))).eval_integ();
								//GPathint[GPath[u_, v__, t_,x_]*((g_.) + (h_.)*(t_))^(n_.), t_, x_] := (1*(g + h*t)^(n + 1)*GPath[u, v,t, x])/((n + 1)*h)
     							// - GPathint[Expand[Apart[(1*(g + h*t)^(n + 1)*GPath[v, t,x])/((n + 1)*h*(t - u)), t]], t, x] /; n != -1 && FreeQ[{g, h}, t]
							}

						}
					} else if (exps[0] == 1) // exponents are exponents of denominators.
					{
						if (is_a<lst>(trans.op(0)))
						{
							lst args = ex_to<lst>(trans.op(0));
							return prefactor*GPath(args.prepend(offsets[0]),trans.op(1),trans.op(2));
						}
					} 
				}
			}
			
			
		} else if (exps.size() > 1)
		{
			cout << "Flag unexpected number of factors. f is " << f << " \n";
			return *this;
		}
		

	}
	

	

}

GINAC_BIND_UNARCHIVER(pintegral);
} // namespace GiNaC
