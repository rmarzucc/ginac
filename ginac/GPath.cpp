/** @file GPath.cpp
 *
 *  Implementation of GiNaC's symbolic pathintegral. */

/*
 *  GiNaC Copyright (C) 1999-2020 Johannes Gutenberg University Mainz, Germany
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "GPath.h"
#include "numeric.h"
#include "symbol.h"
#include "add.h"
#include "mul.h"
#include "power.h"
#include "inifcns.h"
#include "wildcard.h"
#include "archive.h"
#include "registrar.h"
#include "utils.h"
#include "operators.h"
#include "relational.h"

using namespace std;

namespace GiNaC {

GINAC_IMPLEMENT_REGISTERED_CLASS_OPT(GPath, basic,
  print_func<print_dflt>(&GPath::do_print).
  print_func<print_python>(&GPath::do_print).
  print_func<print_latex>(&GPath::do_print_latex))


//////////
// default constructor
//////////

GPath::GPath()    //Not sure what exactly we should do here
		: z(dynallocate<symbol>())
{}

//////////
// other constructors
//////////

// public

GPath::GPath(const ex & args_, const ex & z_, const ex & path_)
		:  args(args_), z(z_), path(path_)
{
	if (!is_a<lst>(args_)) {
		throw(std::invalid_argument("first argument must be a list"));
	}

	if (!is_a<lst>(path_)) {
		throw(std::invalid_argument("last argument must be a list"));
	}
}

//////////
// archiving
//////////

void GPath::read_archive(const archive_node& n, lst& sym_lst)
{
	inherited::read_archive(n, sym_lst);
	n.find_ex("args", args, sym_lst);
	n.find_ex("z", z, sym_lst);
	n.find_ex("path", path, sym_lst);
}

void GPath::archive(archive_node & n) const
{
	inherited::archive(n);
	n.add_ex("args", args);
	n.add_ex("z", z);
	n.add_ex("path", path);
}

//////////
// functions overriding virtual functions from base classes
//////////

void GPath::do_print(const print_context & c, unsigned level) const
{
	c.s << "GPath(";
	args.print(c);
	c.s << ",";
	z.print(c);
	c.s << ",";
	path.print(c);
	c.s << ")";
}

void GPath::do_print_latex(const print_latex & c, unsigned level) const //Still need to implement this properly
{
	string varname = ex_to<symbol>(args).get_name();
	c.s << "\\tilde_{\\Gamma}\\left(";
	args.print(c);
	c.s << ",";
	z.print(c);
	c.s << ";";
	path.print(c);
	c.s << "\\right)";
}

int GPath::compare_same_type(const basic & other) const
{
	GINAC_ASSERT(is_exactly_a<GPath>(other));
	const GPath &o = static_cast<const GPath &>(other);

	int cmpval = args.compare(o.args);
	if (cmpval)
		return cmpval;
	cmpval = z.compare(o.z);
	if (cmpval)
		return cmpval;
	return path.compare(o.path);
}

ex GPath::eval() const
{
	if (flags & status_flags::evaluated)
		return *this;

		//Maybe replace GPath[{{0,0}},z;tau] -> z

	// if (!f.has(x) && !haswild(f))
	// 	return x*f-b.op(0)*f;

	//if (b.op(0)==b.op(1))
	//	return _ex0;

	return this->hold();
}

// ex GPath::evalf() const
// {
// 	lst repL = lst{z1,z2,z3,z,tt};
// 	lst repR = lst{1/6 + 1/6*I, 1/4 + 1/4*I, 1/2 + 1/2*I, 1/2, I};
// 	ex expr = GPath(args,z,tau);

// 	return expr.subs(repL,repR);

// }


// lst permute(int i, lst s, lst pp)
// {
// 	lst perms = pp;

//     // base case
//     if (i == (s.nops() - 1)) {
//         perms.append(s);
//         return perms;
//     }

// 	symbol x(x);
   
//     ex prev = x;
   
//     // loop from j = 1 to length of string
//     for (int j = i; j < s.nops(); j++) {
//         lst temp = s;
//         if (j > i && temp[i] == temp[j])
//             continue;
//         if (prev != x && prev == s[j]) {
//             continue;
//         }
       
//         // swap the elements
//         swap(temp[i], temp[j]);
//         prev = s[j];
       
//         // recursion call
//         return permute(i + 1, temp, pp);
//     }
// }

exvector GPath::lst_to_vec(const lst l) const
{
	exvector vec;
	vec.reserve(l.nops());

	for (lst::const_iterator i = l.begin(); i != l.end(); ++i)
        vec.push_back(*i);

	return vec;
}

lst GPath::reverse_list(const lst l) const
{
	lst rev;

	for (auto i = l.rbegin(); i != l.rend(); ++i)
        rev.append(*i);

	return rev;
}

void GPath::vec_to_lst(const exvector vec, lst l) const
{
	l.remove_all();

	for (auto i = vec.begin(); i != vec.end(); ++i)
        l.append(*i);

}

lst GPath::int_part0(const int nn, const int mm) const
{
	lst decompositions, permutations;
	lst hlp, dec;
	
	for (size_t i = 0; i < mm; i++)
	{
		hlp.append(0);
	}
	
	int k = 1;
	ex x,y;
	hlp[1] = nn;
	//decompositions.append(hlp);
	while (k != 0)
	{
		x = hlp[k-1] + 1;
		y = hlp[k] - 1;
		k -= 1;
		while (x <= y && k < mm-1)
		{
			hlp[k] = x;
			y -= x;
			k += 1;
		}
		hlp[k] = x + y;

		for (size_t i = 0; i < k+1; i++)
		{
			dec.append(hlp[i]);
		}

		for (size_t i = k+1; i < mm; i++)
		{
			dec.append(0);
		}
		

		decompositions.append(dec);
		dec.remove_all();
		
	}

	exvector permute;
	lst perm_lst;

	for (size_t i = 0; i < decompositions.nops(); i++)
	{
		permute = lst_to_vec(ex_to<lst>(decompositions.op(i)));
		std::sort (permute.begin(), permute.end());

		do
		{
			for (size_t i = 0; i < permute.size(); i++)
			{
				perm_lst.append(permute[i]);
			}
		
			permutations.append(perm_lst);
			perm_lst.remove_all();
		} while ( next_permutation(permute.begin(), permute.end()) );
	}
	return permutations;
	
}


// lst GPath::int_part0(const int nn, const int mm) const
// {
// 	lst decompositions, permutations;
// 	lst hlp;
	
// 	for (size_t i = 0; i < mm; i++)
// 	{
// 		hlp.append(0);
// 	}
	
// 	int k = 1;
// 	ex x,y;
// 	hlp[1] = nn;
// 	decompositions.append(hlp);
// 	while (k != 0)
// 	{
// 		x = hlp[k-1] + 1;
// 		y = hlp[k] - 1;
// 		k -= 1;
// 		while (x <= y && k < mm-1)
// 		{
// 			hlp[k] = x;
// 			y -= x;
// 			k += 1;
// 			hlp[k] = x + y;
// 			decompositions.append(hlp);
// 		}
		
// 	}

// 	exvector permute;
// 	lst perm_lst;

// 	for (size_t i = 0; i < decompositions.nops(); i++)
// 	{
// 		permute = lst_to_vec(ex_to<lst>(decompositions.op(i)));
// 		std::sort (permute.begin(), permute.end());

// 		do
// 		{
// 			for (size_t i = 0; i < permute.size(); i++)
// 			{
// 				perm_lst.append(permute[i]);
// 			}
		
// 			permutations.append(perm_lst);
// 			perm_lst.remove_all();
// 		} while ( next_permutation(permute.begin(), permute.end()) );
// 	}
// 	return permutations;
	
// }



ex GPath::dec_path() const
{
	lst steps, kers_per_step;
	size_t m = path.nops() - 1;
	lst reverse_args = reverse_list(ex_to<lst>(args));

	for (size_t i = 0; i < m; i++)
	{
		steps.append(lst{path.op(i),path.op(i+1)});
	}
	
	kers_per_step = int_part0(args.nops(),m);

	ex decomposition;
	ex prod;
	size_t counter = 0;
	lst arg_hlp;

	for (size_t i = 0; i < kers_per_step.nops(); i++) // Iterate over all permutations
	{
		prod = 1;
		for (size_t j = 0; j < m; j++) // Iterate over all steps 
		{
			size_t n = to_long( ex_to<numeric>( kers_per_step.op(i).op(j) ) );

			if (n > 0)
			{
				// for (auto i = reverse_args.begin(); i != reverse_args.begin()+n; ++i)
        		// 	arg_hlp.append(*i);

				for (size_t k = counter; k < counter + n; k++) // Iterate over the arguments
				{
					arg_hlp.append(reverse_args.op(k));
				}

				counter += n;
				prod = prod * GPath(reverse_list(arg_hlp),z,steps.op(j));
				arg_hlp.remove_all();
				arg_hlp.remove_all();
			}
		}
		counter = 0;

		decomposition += prod;
		
	}
	

	return decomposition;

}


ex GPath::evalf() const
{

	if (path.nops() == 2)
	{
		if (path.op(0) == 0)
		{
			if (args == lst{0} && path.op(1).match( qq(wild(0),wild(1)) ) ) //Fix the branch to Log[exp[x]] = x
			{
				ex rtvalue = 2*Pi*I * path.op(1).op(1) * pow(path.op(1).op(0),-1);
				return rtvalue.evalf();
			}

			return G(args,path.op(1)).evalf();

		} else
		{
			lst newargs;
			ex z2 = path.op(0);
			ex z1 = path.op(1);
			for (size_t i = 0; i < args.nops(); i++)
			{
				newargs.append((args.op(i) - z2)*pow(z1 - z2,-1));
			}
			return G(newargs,1).evalf();
			
		}
		
	} else
	{

		ex hlp = GPath(args,z,path).dec_path();

		return hlp.evalf();
	}
}

// ex psubsvalue(const ex & var, const ex & value, const ex & fun)
// {
// 	ex result = fun.subs(var==value).evalf();
// 	if (is_a<numeric>(result))
// 		return result;
// 	throw logic_error("integrand does not evaluate to numeric");
// }

// int GPath::degree(const ex & s) const
// {
// 	return ((b.op(1)-b.op(0))*f).degree(s);
// }

// int GPath::ldegree(const ex & s) const
// {
// 	return ((b.op(1)-b.op(0))*f).ldegree(s);
// }

// ex GPath::eval_ncmul(const exvector & v) const
// {
// 	return f.eval_ncmul(v);
// }

size_t GPath::nops() const
{
	return 3;
}

ex GPath::op(size_t i) const
{
	GINAC_ASSERT(i<3);

	switch (i) {
		case 0:
			return args;
		case 1:
			return z;
		case 2:
			return path;
		default:
			throw (std::out_of_range("GPath::op() out of range"));
	}
}

ex & GPath::let_op(size_t i)
{
	ensure_if_modifiable();
	switch (i) {
		case 0:
			return args;
		case 1:
			return z;
		case 2:
			return path;
		default:
			throw (std::out_of_range("GPath::let_op() out of range"));
	}
}




ex GPath::derivative(const symbol & s) const
{
	if (s==path.op(path.nops())) 
	{
		lst args2 = ex_to<lst>(args);
		return pow(z-args.op(0),-1)*GPath(args2.remove_first(), z, path);
	} else
	{
		throw logic_error("Only derivations with respect to the endpoint implemented");
	}
	
}

// unsigned GPath::return_type() const
// {
// 	return tau.return_type();
// }

// return_type_t GPath::return_type_tinfo() const
// {
// 	return tau.return_type_tinfo();
// }

ex GPath::conjugate() const
{
	lst args2 = ex_to<lst>(args);
	lst path2 = ex_to<lst>(args);
	lst newargs;
	lst newpath;
	size_t len = args2.nops();
	size_t lenp = path2.nops();
	ex pi;
	ex zi;

	for (size_t i = 0; i < len; i++)
	{
		zi = args.op(i).conjugate();
		newargs.append(zi);
	}

	for (size_t i = 0; i < lenp; i++)
	{
		pi = path.op(i).conjugate();
		newpath.append(pi);
	}
	
	ex conjargs = lst(newargs);
	ex conjpath = lst(newpath);
	ex conjz = z.conjugate();

	if (are_ex_trivially_equal(args, conjargs) && are_ex_trivially_equal(path, conjpath) &&
	    are_ex_trivially_equal(z, conjz))
		return *this;

	return dynallocate<GPath>(conjargs, conjz, conjpath);
}



GINAC_BIND_UNARCHIVER(GPath);
} // namespace GiNaC
