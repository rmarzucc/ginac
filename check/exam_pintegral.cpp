/** @file exam_pintegral.cpp
 *
 *  Tests for symbolic partial fractionning within the scope of cases appearing when q-expanding elliptic polylogs. */

/*
 *  GiNaC Copyright (C) 1999-2020 Johannes Gutenberg University Mainz, Germany
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "ginac.h"
using namespace GiNaC;

#include <iostream>
using namespace std;

ex getFunc(const ex &e)
{
	if (is_a<pintegral>(e)) {
		return e.op(2);
	}

	if (is_a<add>(e))
	{
		exvector v;
		v.reserve(e.nops());
		for (size_t i=0; i<e.nops(); ++i)
			v.push_back(getFunc(e.op(i)));
		return ex(add(v));
	}

	if (is_a<mul>(e))
	{
		exvector v;
		v.reserve(e.nops());
		for (size_t i=0; i<e.nops(); ++i)
			v.push_back(getFunc(e.op(i)));
		return ex(mul(v));
	}

	return e;
}

static unsigned check_pintegral(const ex &e, const ex &f, const lst &subs1, const lst &subs2)
{
	ex ed = ex_to<pintegral>(e).eval_integ();

	if ( !((ed - f).subs(subs1).is_zero() && (ed - f).subs(subs2).is_zero()) ) 
	{
		clog << endl;
		clog << "Pathintegral of " << e << " returned " << ed << "instead of " << f << endl;
		clog << "or, with subs1 " << ed.subs(subs1) << " vs " << f.subs(subs1) << endl;
		clog << "or, with subs2 " << ed.subs(subs2) << " vs " << f.subs(subs2) << endl;
		clog << "returned:" << endl;
		clog << tree << ed << "instead of\n" << dflt;

		return 1;
	}
	return 0;
}

// Monomial integrals weight 0
static unsigned exam_pintegral0()
{
	unsigned result = 0;
	symbol x("x"), a("a"), b("b"), c("c"), p1("p1"), p2("p2");
	lst subs1 = lst{x==1,a==3,b==-7,c==1*inverse(11), p1==71, p2 == 5};
	lst subs2 = lst{x==-1*inverse(2),a==33,b==7*inverse(8),c==1, p1 == 23, p2 == 77};
	ex xx, e1, e2, e3, e4, e, f, path;
	
	xx = x;
	e1 = x;
	e2 = x-a;
	e3 = x-b;
	e4 = 2*x-b;
	path = lst{p1,p2};

	#include "exam_pintegral_hlp0.txt"
	
	return result;
}

// Monomial integrals weight 1
static unsigned exam_pintegral1()
{
	unsigned result = 0;
	symbol x("x"), a("a"), b("b"), c("c"), p1("p1"), p2("p2");
	lst subs1 = lst{x==1,a==3,b==-7,c==1*inverse(11), p1==71, p2 == 5};
	lst subs2 = lst{x==-1*inverse(2),a==33,b==7*inverse(8),c==1, p1 == 23, p2 == 77};
	ex xx, e1, e2, e3, e4, e, f, path;
	
	xx = x;
	e1 = x;
	e2 = x-a;
	e3 = x-b;
	e4 = 2*x-b;
	path = lst{p1,p2};

	#include "exam_pintegral_hlp1.txt"
	
	return result;
}

// Monomial integrals weight 2
static unsigned exam_pintegral2()
{
	unsigned result = 0;
	symbol x("x"), a("a"), b("b"), c("c"), p1("p1"), p2("p2");
	lst subs1 = lst{x==1,a==3,b==-7,c==1*inverse(11), p1==71, p2 == 5};
	lst subs2 = lst{x==-1*inverse(2),a==33,b==7*inverse(8),c==1, p1 == 23, p2 == 77};
	ex xx, e1, e2, e3, e4, e, f, path;
	
	xx = x;
	e1 = x;
	e2 = x-a;
	e3 = x-b;
	e4 = 2*x-b;
	path = lst{p1,p2};

	#include "exam_pintegral_hlp2.txt"
	
	return result;
}



unsigned exam_pintegral()
{
	unsigned result = 0;
	
	cout << "examining pintegral0" << endl << flush;
	result += exam_pintegral0();  cout << '.' << flush;
	cout << "examining pintegral1" << endl << flush;
	result += exam_pintegral1();  cout << '.' << flush;
	cout << "examining pintegral2" << endl << flush;
	result += exam_pintegral2();  cout << '.' << flush;
	//result += exam_apart2();  cout << '.' << flush;
	// result += exam_differentiation2();  cout << '.' << flush;
	// result += exam_differentiation3();  cout << '.' << flush;
	// result += exam_differentiation4();  cout << '.' << flush;
	// result += exam_differentiation5();  cout << '.' << flush;
	// result += exam_differentiation6();  cout << '.' << flush;
	// result += exam_differentiation7();  cout << '.' << flush;
	// result += exam_differentiation8();  cout << '.' << flush;
	
	return result;
}

int main(int argc, char** argv)
{
	return exam_pintegral();
}
