/** @file exam_apart.cpp
 *
 *  Tests for symbolic partial fractionning within the scope of cases appearing when q-expanding elliptic polylogs. */

/*
 *  GiNaC Copyright (C) 1999-2020 Johannes Gutenberg University Mainz, Germany
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "ginac.h"
using namespace GiNaC;

#include <iostream>
using namespace std;

ex getFunc(const ex &e)
{
	if (is_a<pintegral>(e)) {
		return e.op(2);
	}

	if (is_a<add>(e))
	{
		exvector v;
		v.reserve(e.nops());
		for (size_t i=0; i<e.nops(); ++i)
			v.push_back(getFunc(e.op(i)));
		return ex(add(v));
	}

	if (is_a<mul>(e))
	{
		exvector v;
		v.reserve(e.nops());
		for (size_t i=0; i<e.nops(); ++i)
			v.push_back(getFunc(e.op(i)));
		return ex(mul(v));
	}

	return e;
}

static unsigned check_apart(const ex &e, const lst subs1, const lst subs2)
{
	ex ed = ex_to<pintegral>(e).apart();
	ex f1 = e.op(2);
	ex f2 = getFunc(ed);

	if ( !( (f1 - f2).subs(subs1).is_zero() && (f1 - f2).subs(subs2).is_zero() ) ) 
	{
		clog << endl;
		clog << "Apart of " << e << " returned " << ed << endl;
		clog << "or, without the wrapper, " << f1 << " vs " << f2 << endl;
		clog << "or, with subs1, " << f1.subs(subs1) << " vs " << f2.subs(subs1) << endl;
		clog << "or, with subs2, " << f1.subs(subs2) << " vs " << f2.subs(subs2) << endl;
		clog << "returned:" << endl;
		clog << tree << ed << "instead of\n" << dflt;

		return 1;
	}
	return 0;
}

// Simple (expanded) polynomials
static unsigned exam_apart1()
{
	unsigned result = 0;
	symbol x("x"), a("a"), b("b");
	lst subs1 = lst{x==1,a==3,b==-7};
	lst subs2 = lst{x==-1*inverse(2),a==33,b==7*inverse(8)};
	ex xx, e1, e2, e3, e, d, path;
	
	xx = x;
	e1 = x;
	e2 = x-a;
	e3 = x-b;
	path = lst{a,b};

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-0)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-0)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-0)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-0)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-0)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-0)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-1)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-1)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-1)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-1)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-1)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-1)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-2)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-2)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-2)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-2)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-2)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-2)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-3)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-3)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-3)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-3)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-3)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-3)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-4)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-4)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-4)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-4)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-4)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-4)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-5)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-5)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-5)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-5)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-5)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,0)*pow(e2,-5)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-0)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-0)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-0)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-0)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-0)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-0)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-1)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-1)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-1)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-1)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-1)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-1)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-2)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-2)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-2)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-2)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-2)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-2)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-3)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-3)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-3)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-3)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-3)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-3)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-4)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-4)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-4)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-4)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-4)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-4)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-5)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-5)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-5)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-5)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-5)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,1)*pow(e2,-5)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-0)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-0)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-0)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-0)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-0)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-0)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-1)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-1)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-1)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-1)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-1)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-1)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-2)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-2)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-2)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-2)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-2)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-2)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-3)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-3)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-3)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-3)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-3)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-3)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-4)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-4)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-4)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-4)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-4)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-4)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-5)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-5)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-5)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-5)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-5)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,2)*pow(e2,-5)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-0)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-0)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-0)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-0)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-0)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-0)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-1)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-1)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-1)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-1)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-1)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-1)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-2)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-2)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-2)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-2)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-2)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-2)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-3)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-3)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-3)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-3)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-3)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-3)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-4)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-4)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-4)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-4)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-4)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-4)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-5)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-5)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-5)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-5)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-5)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,3)*pow(e2,-5)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-0)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-0)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-0)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-0)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-0)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-0)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-1)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-1)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-1)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-1)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-1)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-1)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-2)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-2)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-2)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-2)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-2)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-2)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-3)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-3)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-3)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-3)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-3)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-3)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-4)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-4)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-4)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-4)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-4)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-4)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-5)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-5)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-5)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-5)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-5)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,4)*pow(e2,-5)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-0)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-0)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-0)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-0)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-0)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-0)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-1)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-1)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-1)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-1)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-1)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-1)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-2)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-2)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-2)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-2)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-2)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-2)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-3)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-3)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-3)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-3)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-3)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-3)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-4)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-4)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-4)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-4)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-4)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-4)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-5)*pow(e3,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-5)*pow(e3,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-5)*pow(e3,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-5)*pow(e3,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-5)*pow(e3,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,5)*pow(e2,-5)*pow(e3,-5));
	result += check_apart(e,subs1,subs2);
	
	return result;
}

static unsigned exam_apart2()
{
	unsigned result = 0;
	symbol x("x"), a("a"), b("b");
	lst subs1 = lst{x==1,a==3,b==-7};
	lst subs2 = lst{x==-1*inverse(2),a==33,b==7*inverse(8)};
	ex xx, e1, e2, e3, e, d, path;
	
	xx = x;
	e1 = x;
	e2 = x-a;
	e3 = x-b;
	path = lst{a,b};

	


	e=pintegral(xx,path,pow(e1,-1)*pow(e2,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-1)*pow(e2,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-1)*pow(e2,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-1)*pow(e2,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-1)*pow(e2,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-1)*pow(e2,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-2)*pow(e2,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-2)*pow(e2,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-2)*pow(e2,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-2)*pow(e2,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-2)*pow(e2,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-2)*pow(e2,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-3)*pow(e2,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-3)*pow(e2,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-3)*pow(e2,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-3)*pow(e2,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-3)*pow(e2,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-3)*pow(e2,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-4)*pow(e2,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-4)*pow(e2,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-4)*pow(e2,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-4)*pow(e2,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-4)*pow(e2,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-4)*pow(e2,-5));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-5)*pow(e2,-0));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-5)*pow(e2,-1));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-5)*pow(e2,-2));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-5)*pow(e2,-3));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-5)*pow(e2,-4));
	result += check_apart(e,subs1,subs2);

	e=pintegral(xx,path,pow(e1,-5)*pow(e2,-5));
	result += check_apart(e,subs1,subs2);

	
	return result;
}

unsigned exam_apart()
{
	unsigned result = 0;
	
	cout << "examining apart" << flush;
	
	result += exam_apart1();  cout << '.' << flush;
	result += exam_apart2();  cout << '.' << flush;
	// result += exam_differentiation2();  cout << '.' << flush;
	// result += exam_differentiation3();  cout << '.' << flush;
	// result += exam_differentiation4();  cout << '.' << flush;
	// result += exam_differentiation5();  cout << '.' << flush;
	// result += exam_differentiation6();  cout << '.' << flush;
	// result += exam_differentiation7();  cout << '.' << flush;
	// result += exam_differentiation8();  cout << '.' << flush;
	
	return result;
}

int main(int argc, char** argv)
{
	return exam_apart();
}
